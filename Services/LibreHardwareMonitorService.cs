#region License
// Wee Hardware Stat Server
// Copyright (C) 2021 Vinod Mishra and contributors
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; If not, see <http://www.gnu.org/licenses/>.
#endregion

using LibreHardwareMonitor.Hardware;
using System.Globalization;
using System;
using WeeHardwareStatServer.Models;
using WeeHardwareStatServer.Services.Interfaces;

namespace WeeHardwareStatServer.Services
{
    public class LibreHardwareMonitorService : IHardwareMonitorService
    {
        private readonly IComputer _computer;
        private readonly IVisitor _visitor;

        public LibreHardwareMonitorService(
            IComputer computer,
            IVisitor visitor)
        {
            _computer = computer;
            _visitor = visitor;
        }

        public HardwareInfo GetStats()
        {
            _computer.Accept(_visitor);
            var result = new HardwareInfo();
            foreach (var hardware in _computer.Hardware)
            {
                switch (hardware.HardwareType)
                {
                    case HardwareType.Cpu:
                        result.CpuName = hardware.Name;
                        break;
                    case HardwareType.GpuAmd:
                    case HardwareType.GpuNvidia:
                        //LibreHardwareMonitor is broken and can output NVIDIA twice in the name so temporary fix for now
                        result.GpuName = hardware.Name.Replace("NVIDIA NVIDIA", "NVIDIA");
                        break;
                }

                foreach (var sensor in hardware.Sensors)
                {
                    if (sensor.Value != null)
                    {
                        switch ((sensor.Name, sensor.SensorType, hardware.HardwareType))
                        {
                            case ("GPU Memory Total", SensorType.SmallData, _):
                                var gpuMemoryTotal = Math.Round((decimal)sensor.Value, 0);
                                result.GpuMemoryTotal =
                                    gpuMemoryTotal.ToString(CultureInfo.InvariantCulture);
                                break;
                        }
                    }
                }
            }

            return result;
        }
    }
}